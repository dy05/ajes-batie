<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tabs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->enum('tab', ['school', 'experience', 'skill', 'community'])->default('school');
            $table->enum('type', ['internship', 'work'])->default('internship');
            $table->boolean('is_active')->default(1);
            $table->string('name')->nullable();
            $table->string('society')->nullable();
            $table->string('society_address')->nullable();
            $table->json('level')->nullable();
            $table->text('description')->nullable();
            $table->text('avatar_link')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->on('users')->references('id')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tabs');
    }
}
