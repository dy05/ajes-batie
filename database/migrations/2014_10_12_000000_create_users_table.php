<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->string('address')->nullable();
            $table->string('address_display')->nullable();
            $table->string('cni')->nullable();
            $table->string('born_place')->nullable();
            $table->boolean('is_super_admin')->default(0);
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_moderator')->default(0);
            $table->boolean('is_blogger')->default(0);
            $table->json('languages')->nullable();
            $table->json('pleasures')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('born_at')->nullable();
            $table->string('password');
            $table->string('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->text('description')->nullable();
            $table->text('avatar_link')->nullable();
            $table->timestamp('ready_at')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->timestamp('cni_at')->nullable();
            $table->timestamp('phone_at')->nullable();
            $table->timestamp('address_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
