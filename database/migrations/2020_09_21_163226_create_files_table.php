<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->string('name')->unique()->nullable();
            $table->string('file_name')->unique()->nullable();
            $table->string('path_link')->unique();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_video')->default(1);
            $table->morphs('item');
            $table->string('domains')->nullable();
            $table->enum('type', ['default', 'cni', 'address', 'phone'])->default('default');
            $table->date('date')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->on('users')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
