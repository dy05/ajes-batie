<?php

namespace App\Observers;

use App\Models\Album;
use App\Models\Post;
use App\Models\Tag;

class TaggableObserver
{
    /**
     * @param Album|Post $item
     */
    public function deleting($item)
    {
        $item->tags()->decrement('count_use', 1);
        Tag::unusedTag();
    }
}
