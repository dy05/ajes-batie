<?php

namespace App\Traits;

use App\Models\Tag;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

trait Taggable {
    public static function bootTaggable()
    {
        static::observe(\App\Observers\TaggableObserver::class);
    }

    /**
     * @return BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @param null|string $tags
     */
    public function saveTags(?string $tags)
    {
        $tags = array_map(function ($tag) { return trim($tag); },
            array_unique(explode(',', mb_strtolower($tags))));
        $tags = array_filter($tags, function ($tag) { return !empty($tag); });

        $presentsTags = Tag::whereIn('name', $tags)->get();

        $tags = array_diff($tags, $presentsTags->pluck('name')->all());

        $newTags = array_map(function ($tag) {
            return [
                'name' => mb_strtolower($tag),
                'slug' => Str::slug($tag),
                'count_use' => 1
            ];
        }, $tags);

        $createdTags = $this->tags()->createMany($newTags);

        $presentsTags = $presentsTags->merge($createdTags);
        $editsTag = $this->tags()->sync($presentsTags);

        Tag::whereIn('id', $editsTag['attached'])->increment('count_use', 1);
        Tag::whereIn('id', $editsTag['detached'])->decrement('count_use', 1);
        Tag::unusedTag();
    }

    public function getTagsListAttribute()
    {
        return $this->tags()->pluck('name')->implode(', ');
    }
}
