<?php

namespace App\Traits;

trait Activable {

    /**
     * Retourne le statut actif d'un enregistrement
     *
     * @return string
     */
    public function getActive()
    {
        return ($this->is_active === 1) ? __('Yes') : __('No');
    }
}
