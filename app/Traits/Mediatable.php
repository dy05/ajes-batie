<?php

namespace App\Traits;

use App\Models\Media;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Trait Mediatable
 *
 * Ce trait permettra de rajouter des elements pour les models prenant des images
 *
 * @package App\Traits
 */
trait Mediatable
{
}
