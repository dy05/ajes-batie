<?php

namespace App\Models;

use App\Traits\Activable;
use App\Traits\Taggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory, Activable, Taggable;

    protected $fillable = ['name', 'description', 'is_active'];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function files()
    {
        return $this->hasMany(File::class)
            ->where('item_name', '=', self::class)
            ->where('item_id', '=', $this->id);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)
            ->where('item_name', '=', self::class)
            ->where('item_id', '=', $this->id);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getMediaAttribute()
    {
        return $this->files()->where('is_active', '=', 1)->first();
    }
}
