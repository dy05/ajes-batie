<?php

namespace App\Models;

use App\Traits\Activable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, Activable;

    protected $fillable = ['title', 'content', 'user_id', 'is_active'];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function files()
    {
        return $this->hasMany(File::class)
            ->where('item_name', '=', self::class)
            ->where('item_id', '=', $this->id);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)
            ->where('item_name', '=', self::class)
            ->where('item_id', '=', $this->id);
    }

    public function getMediaAttribute()
    {
        return $this->files()->where('is_active', '=', 1)->first();
    }
}
