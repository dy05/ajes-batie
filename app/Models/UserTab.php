<?php

namespace App\Models;

use App\Traits\Activable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTab extends Model
{
    use HasFactory, Activable;

    protected $casts = [
        'is_active' => 'boolean',
        'start_at' => 'datetime',
        'end_at' => 'datetime'
    ];

    /*
     * type = enum (stage, work)
     * tab = enum (school, experience, skill, community)
     */
    protected $fillable = ['tab', 'start_at', 'end_at', 'user_id', 'type', 'society',
        'society_address', 'description', 'level', 'is_active', 'avatar_link', 'name'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
