<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $table = 'params';

    // profils_number ici fait reference au nombre de profil a afficher quand le recruteur n'est pas connecte
    protected $fillable = ['active_post_id', 'profils_number', 'hidden_admins',
        'testimonies_number', 'testimonies_slides_number'];
}
