<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     * languages = json(name, level(enum[A1, A2, B1, B2, C1]))
     * pleasures = json([name, icon_name, icon_type])
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'born_at',
        'description',
        'avatar_link',
        'ready_at',
        'cni',
        'cni_at',
        'phone',
        'phone_at',
        'address',
        'address_at',
        'address_display',
        'born_place',
        'linkedin_link',
        'facebook_link',
        'twitter_link',
        'pleasures',
        'languages',
        'is_super_admin',
        'is_admin',
        'is_moderator',
        'is_blogger',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'cni_at' => 'datetime',
        'born_at' => 'datetime',
        'ready_at' => 'datetime',
        'phone_at' => 'datetime',
        'address_at' => 'datetime',
        'languages' => 'json',
        'pleasures' => 'json',
        'is_super_admin' => 'boolean',
        'is_admin' => 'boolean',
        'is_moderator' => 'boolean',
        'is_blogger' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function getNameAttribute()
    {
        return empty($this->last_name) ?
            $this->first_name : $this->first_name.' '.$this->last_name;
    }
}
