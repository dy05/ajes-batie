<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'count_use'];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_tag');
    }

    public function albums()
    {
        return $this->belongsToMany(Album::class, 'album_tag');
    }

    public static function unusedTag()
    {
        return static::where('count_use', 0)->delete();
    }
}
