<?php

namespace App\Models;

use App\Traits\Activable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory, Activable;

    protected $casts = [
        'is_active' => 'boolean',
        'is_video' => 'boolean',
        'date' => 'date'
    ];

    protected $type_enum = ['default', 'cni', 'address', 'phone'];

    protected $fillable = ['name', 'filename', 'size', 'is_video', 'item_id',
        'item_name', 'description', 'date', 'is_active', 'type', 'path_link'];

    public function item()
    {
        return $this->morphTo('item');
    }
}
