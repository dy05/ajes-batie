<?php

namespace App\Models;

use App\Traits\Activable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    use HasFactory, Activable;

    protected $casts = [
        'is_active' => 'boolean',
        'domains' => 'array'
    ];

    protected $fillable = ['is_active', 'code', 'duration', 'parent_id', 'name',
        'domains', 'phone', 'email', 'avatar_link', 'facebook_link', 'twitter_link', 'linkedin_link'];
}
