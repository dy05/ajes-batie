<?php

namespace App\Models;

use App\Traits\Activable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    use HasFactory, Activable;

    protected $fillable = ['avatar_link', 'name', 'content', 'facebook_link',
        'twitter_link', 'linkedin_link', 'ip_address', 'user_id', 'is_active'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
