@extends('pages.layout')

@section('content')
  <div class="container py-5">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <h1 class="text-primary">Register</h1>
        <hr class="bg-primary" style="height: 5px;">
        @if (Session::has('error'))
          <div class="alert alert-danger">
            {{ Session::get('error', 'Une erreur est survenue !') }}
          </div>
        @endif
        <div class="row py-5">
          <form method="POST" class="col-12" action="/register">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="first_name">{{ __('Firstname') }}</label>
                  <input type="text" id="first_name" name="first_name"
                         class="form-control @if($errors->has('first_name')) is-invalid @endif" value="{{ old('first_name') }}"
                         required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="last_name">{{ __('Lastname') }}</label>
                  <input type="text" id="last_name" name="last_name"
                         class="form-control @if($errors->has('last_name')) is-invalid @endif" value="{{ old('last_name') }}"/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">{{ __('Email') }}</label>
                  <input type="email" id="email" name="email" class="form-control @if($errors->has('email')) is-invalid @endif"
                         value="{{ old('email') }}" required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tel">{{ __('tel') }}</label>
                  <input type="tel" id="tel" name="tel" class="form-control @if($errors->has('tel')) is-invalid @endif"
                         value="{{ old('tel') }}" required/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="born_date">{{ __('born_date') }}</label>
                  <input type="date" id="born_date" name="born_date" class="form-control @if($errors->has('born_date')) is-invalid @endif"
                         value="{{ old('born_date') }}" required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="born_place">{{ __('born_place') }}</label>
                  <input type="text" id="born_place" name="born_place" class="form-control @if($errors->has('born_place')) is-invalid @endif"
                         value="{{ old('born_place') }}" required/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="address">{{ __('address') }}</label>
                  <input type="text" id="address" name="address" class="form-control @if($errors->has('address')) is-invalid @endif"
                         value="{{ old('address') }}" required/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="password">{{ __('Password') }}</label>
                  <input type="password" id="password" name="password"
                         class="form-control @if($errors->has('password')) is-invalid @endif" required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="password_confirmation">{{ __('Confirm Password') }}</label>
                  <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" required/>
                </div>
              </div>
            </div>
            <div class="row justify-content-end">
              <div class="col-md-3">
                <button class="btn btn-outline-secondary w-100 py-3" type="reset">{{ __('Reset') }}</button>
              </div>
              <div class="col-md-3">
                <button class="btn btn-primary w-100 py-3 mt-2 mt-md-0" type="submit">{{ __('Validate') }}</button>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
@endsection
