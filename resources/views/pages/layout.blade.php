<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">

  <!-- Styles -->
  <link rel="stylesheet" href="/css/bootstrap.css">
</head>
<body>

  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-primary">
      <a href="/" class="navbar-brand text-white">
        Ajes Batié
      </a>
    </nav>
  </header>

  <main>
    @yield('content')
  </main>

  @section('js_scripts_after')@show

</body>
</html>
